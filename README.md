# @buynomics/bn-config

  * `reflect-metadata`: make sure to import this once before everything else (in `index.ts`)
  * Runtime vs dev time validation
    - [class-validator](https://github.com/typestack/class-validator) for runtime validation
    - `typescript` classes for dev time validation
    - Nested validation is a bit weird, but works ok
  * [class-transformer](https://github.com/typestack/class-transformer)
  * `tsconfig`
    - `emitDecoratorMetadata`
    - `experminetalDecorators`
  * `tslint`
    * `max-classes-per-file: false`: need this for nested validations (unless you want to create a different file for each nested config class)
  * `default.yml`
    - no secrets (because which key would it use?)
  * use types from typescript definitions for validation
    - ex: `bunyan` config options are validated against the `bunyan` type declarations provided by `@types/bunyan` (`config.ts` line 13)
  * encryption: `sops` still works, but uses `NODE_ENV` to choose the correct config file
  * only `yaml` is accepted at this time
  * also accepts **AWS SSM Parameter Store** variables when prefixed with `ps://` (works only for buynomics currently)
  * substitutes environment variables with the pattern `${(\w+)}/g` (`default.yml` line 6)
  * can use `.env` to define variables if required


## Evaluation Flow

  1. Load the default config from `default.yml`
  1. Load the override config from `${NODE_ENV}.yml`
  1. Decrypt the override config
  1. Merge the default config and override config
  1. Flatten the merged structure (necessary for AWS SSM parameters)
  1. Replace all variables starting with `Secret` as `replace(/Secret/g, '')` (so that we don't have to do `{...config.database, password: config.database.passwordSecret}` everywhere)
  1. Unflatten the structure
  1. Substitute environment variables with `replace(/\${(\w+)}/g, process.env[$1])`
  1. Convert plain config object to instance of `Config` class with `plainToClass`
  1. Validate the instance of `Config` class with `class-validator`
  1. Throw errors in plain English if validation fails
  1. If validation passes, return the instance of the `Config` class

  Source is available at `node_modules/@buynomics/bn-config/src/index.ts`