import { loadConfig } from '@buynomics/bn-config'
import bunyan from 'bunyan'
import 'reflect-metadata'
import { Config } from './config'
const config = loadConfig(Config)
const logger = bunyan.createLogger(config.logger)
logger.info(config)
