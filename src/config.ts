import { LoggerOptions } from 'bunyan'
import { Type } from 'class-transformer'
import { IsDefined, IsNumber, Max, Min, ValidateNested } from 'class-validator'
class Server {
  @IsNumber()
  @Min(1024)
  @Max(65535)
  public port: number = 8080
}

class Database {
  @IsDefined()
  public password: string
}

export class Config {
  @IsDefined()
  public logger: LoggerOptions

  @IsDefined()
  @ValidateNested()
  @Type(() => Server)
  public server: Server

  @IsDefined()
  @ValidateNested()
  @Type(() => Database)
  public database: Database
}
